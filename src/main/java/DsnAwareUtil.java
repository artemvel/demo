import org.slf4j.MDC;

public class DsnAwareUtil {

    private static final String DSN_KEY = "DSN";

    public static <T> T doProcess(String dsn, Action<T> action) {
        try {
            MDC.put(DSN_KEY, dsn);
            return action.apply();
        }finally {
            MDC.clear();
        }
    }

    @FunctionalInterface
    public interface Action<T> {
        T apply();
    }
}
