import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Component;

@Component
public class CustomerS3ObjectIdSha256 {

    private static final DigestUtils DIGEST_UTILS = new DigestUtils("SHA-256");

    public String generateObjectId(String identifier, String id) {
        String clientIdentifier = (id + "." + identifier).toLowerCase();

        return DIGEST_UTILS.digestAsHex(clientIdentifier);
    }
}
