import com.amazonaws.services.s3.AmazonS3;

import java.util.concurrent.ExecutorService;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class CustomerS3ConnectorPrimary extends S3Connector {

    public CustomerS3ConnectorPrimary(
            @Qualifier("s3ClientPrimary") AmazonS3 s3Client,
            @Value("${x.s3.bucket.primary}") String bucketName,
            ExecutorService s3DownloadThreadPool) {
        super(s3Client, bucketName, s3DownloadThreadPool);
    }
}