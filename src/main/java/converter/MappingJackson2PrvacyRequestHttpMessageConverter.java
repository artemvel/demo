package converter;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.json.AbstractJackson2HttpMessageConverter;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;

@Slf4j
public class MappingJackson2PrvacyRequestHttpMessageConverter extends AbstractJackson2HttpMessageConverter {

    private MetricsCollector metricsCollector;

    public MappingJackson2PrvacyRequestHttpMessageConverter(ObjectMapper objectMapper, MetricsCollector metricsCollector) {
        super(objectMapper);
        this.metricsCollector = metricsCollector;
    }

    @Override
    public boolean canRead(Class<?> clazz, MediaType mediaType) {
        return clazz.getTypeName().equals(PrivacyRequest.class.getTypeName());
    }

    @Override
    public boolean canRead(Type type, Class<?> contextClass, MediaType mediaType) {
        return type.getTypeName().equals(PrivacyRequest.class.getTypeName());
    }

    @Override
    public Object read(Type type, Class<?> contextClass, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
        String jsonBody = new String(inputMessage.getBody().readAllBytes(), StandardCharsets.UTF_8);
        try {
            return AvroJsonConverterUtils.toAvro(jsonBody, PrivacyRequest.getClassSchema());
        } catch (Exception e) {
            metricsCollector.getPrivacyRequestFailureCounter().increment();
            log.error("Failed to deserialize x request", e);
            throw e;
        }
    }

}
