package config;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Configuration
@EnableConfigurationProperties(DisclosureRdfsProperties.class)
public class ServiceConfig {

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return objectMapper;
    }

    @Bean
    public HttpClient httpClient(SSLContext sslContext) {
        return HttpClients.custom()
                .setSSLContext(sslContext)
                .build();
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder, HttpClient httpClient) {
        return restTemplateBuilder.requestFactory(() -> new HttpComponentsClientHttpRequestFactory(httpClient))
                .build();
    }

    @Bean
    public ExecutorService s3DownloadThreadPool(
            @Value("${s3.thread-pool.corePoolSize}") Integer corePoolSize,
            @Value("${s3.thread-pool.maximumPoolSize}") Integer maximumPoolSize,
            @Value("${s3.thread-pool.keepAliveTime}") Long keepAliveTime) {
        return new ThreadPoolExecutor(corePoolSize, maximumPoolSize,
                keepAliveTime, TimeUnit.SECONDS,
                new SynchronousQueue<>());
    }

    @Bean
    public AmazonS3 s3ClientPrimary(@Value("${s3.region.primary}") Regions clientRegion) {
        return AmazonS3ClientBuilder.standard()
                .withRegion(clientRegion)
                .build();
    }

    @Bean
    public AmazonS3 s3ClientSecondary(@Value("${s3.region.secondary}") Regions clientRegion) {
        return AmazonS3ClientBuilder.standard()
                .withRegion(clientRegion)
                .build();
    }
}
