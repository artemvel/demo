import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3ObjectSummary;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
public abstract class S3Connector {

    private AmazonS3 s3Client;
    private String bucketName;
    private ExecutorService s3DownloadThreadPool;

    public S3Connector(AmazonS3 s3Client, String bucketName, ExecutorService s3DownloadThreadPool) {
        this.s3Client = s3Client;
        this.bucketName = bucketName;
        this.s3DownloadThreadPool = s3DownloadThreadPool;
    }

    public Optional<String> getObjectContent(String objectId) {
        try {
            return Optional.ofNullable(s3Client.getObjectAsString(bucketName, objectId));
        } catch (AmazonS3Exception e) {
            if (e.getStatusCode() == HttpStatus.SC_NOT_FOUND) {
                log.error("Object with id [" + objectId +"] not found in [" + bucketName + "] s3 bucket", e);
                return Optional.empty();
            } else {
                log.error("Amazon S3 exception caught.", e);
                throw e;
            }
        }
    }

    public Pair<Long, Optional<String>> getObjectContent(Long accountId, String objectId) {
        try {
            return Pair.of(accountId, Optional.ofNullable(s3Client.getObjectAsString(bucketName, objectId)));
        } catch (AmazonS3Exception e) {
            if (e.getStatusCode() == HttpStatus.SC_NOT_FOUND) {
                log.error("Object with id [" + objectId +"] not found in [" + bucketName + "] s3 bucket", e);
                return Pair.of(accountId, Optional.empty());
            } else {
                log.error("Amazon S3 exception caught.", e);
                throw e;
            }
        }
    }

    public List<Pair<Long, Optional<String>>> getObjectContents(List<Pair<Long, String>> accIdObjectIdPair) {
        List<CompletableFuture<Pair<Long, Optional<String>>>> tasks = accIdObjectIdPair.stream()
                .map(pair -> CompletableFuture.supplyAsync(() -> getObjectContent(pair.getKey(), pair.getValue()), s3DownloadThreadPool))
                .collect(Collectors.toList());

        return tasks.stream()
                .map(CompletableFuture::join)
                .collect(Collectors.toList());
    }

    public void delete(String key) {
        log.debug("Deleting file s3://{}/{}", bucketName, key);
        s3Client.deleteObject(bucketName, key);
    }

    public PutObjectResult save(String key, String content) {
        log.debug("Saving content to s3://{}/{}", bucketName, key);
        return s3Client.putObject(bucketName, key, content);
    }

    public List<String> getTopLevelObjectList() {
        List<String> fileList = new ArrayList<>();

        ListObjectsV2Request req = new ListObjectsV2Request()
                .withBucketName(bucketName)
                .withDelimiter("/");

        ListObjectsV2Result result;

        do {
            result = s3Client.listObjectsV2(req);

            fileList.addAll(result.getObjectSummaries()
                    .stream()
                    .map(S3ObjectSummary::getKey)
                    .collect(Collectors.toList()));

            req.setContinuationToken(result.getContinuationToken());
        } while (result.isTruncated());

        return fileList;
    }

}
