
import java.util.ArrayList;
import java.util.List;

import java.util.Set;

import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;

@Component
@AllArgsConstructor
@Slf4j
public class CustomerProvider {

    private CustomerS3ConnectorPrimary customerS3ConnectorPrimary;
    private CustomerS3ConnectorSecondary customerS3ConnectorSecondary;

    private CustomerS3Parser customerS3Parser;
    private CustomerS3ObjectIdSha256 customerS3ObjectIdSha256;

    private CustomerDbProvider customerDbProvider;

    private MetricsCollector metricsCollector;

    public Optional<Long> getCustomerNameId(String email, String dsn) {
        log.info("Searching customerId in S3 by email and dsn: [{}]", dsn);
        try {
            String objectId = customerS3ObjectIdSha256.generateObjectId(email, dsn);
            Optional<Long> fromS3 = getFromS3(objectId);
            if (fromS3.isPresent()) {
                return fromS3;
            }
        } catch (Exception e) {
            throw new ProviderException("Cant provide customerNameId due to s3 issue", e);
        }

        log.info("ObjectId with such email and dsn does not exists in S3. Searching in Data Base...");
        return customerDbProvider.getCustomerNameId(email, dsn);
    }

    public Optional<Long> getCustomerNameIdFromS3(Long accountId, String dsn) {
        log.info("Searching customerId in S3 by account id and dsn: [{}]", dsn);
        try {
            String objectId = customerS3ObjectIdSha256.generateObjectId(String.valueOf(accountId), dsn);
            return getFromS3(objectId);
        } catch (Exception e) {
            throw new ProviderException("Cant provide customerNameId due to s3 issue", e);
        }
    }

    /**
     * @return list of pair (acc_id, cust_name_id)
     * */
    public List<Pair<Long, Long>> getCustomerNameIdFromDB(Set<Long> accountIds, String dsn) {
        log.info("Searching customerId in DB by account ids and dsn: [{}]", dsn);
        try {
            return customerDbProvider.getCustomerNameIds(accountIds, dsn);
        } catch (Exception e) {
            throw new ProviderException("Cant provide customerNameId due DB issue", e);
        }
    }

    /**
     * @return list of pair (acc_id, cust_id)
     * */
    public List<Pair<Long, Optional<Long>>> getCustomerIdsFromS3(List<Long> accountIds, String dsn) {
        log.info("Searching customerIds in S3 by account ids and dsn: [{}]", dsn);
        try {
            List<Pair<Long, String>> accIdObjectIdPair = accountIds.stream()
                    .map(accId -> Pair.of(accId, customerS3ObjectIdSha256.generateObjectId(String.valueOf(accId), dsn)))
                    .collect(Collectors.toList());

            List<Pair<Long, Optional<String>>> objectContents = customerS3ConnectorPrimary.getObjectContents(accIdObjectIdPair);

            return objectContents.stream()
                    .map(this::parseCustId)
                    .collect(Collectors.toList());

        } catch (Exception e) {
            throw new ProviderException("Cant provide customerNameId due S3 issue", e);
        }
    }

    private Pair<Long, Optional<Long>> parseCustId(Pair<Long, Optional<String>> accIdContentPair) {
        if (accIdContentPair.getValue().isPresent()) {
            return Pair.of(accIdContentPair.getKey(), customerS3Parser.parseCustomerNameId(accIdContentPair.getValue().get()));
        } else {
            return Pair.of(accIdContentPair.getKey(), Optional.empty());
        }
    }

    private Optional<Long> getFromS3(String objectId) {
        return customerS3ConnectorPrimary.getObjectContent(objectId)
                .or(() -> {
                    log.info("Primary bucket does not contain file [{}]. " +
                            "Searching in secondary bucket...", objectId);
                    return customerS3ConnectorSecondary.getObjectContent(objectId);
                })
                .map(objectContent -> {
                    log.info("Found content for given ObjectId in S3");
                    Optional<Long> customerNameId = customerS3Parser.parseCustomerNameId(objectContent);
                    if (customerNameId.isEmpty()) {
                        metricsCollector.getFanLookupRequestWarnCustomerS3Counter().increment();
                        log.warn("customerNameId absent in S3 {}", objectId);
                    }

                    return customerNameId;
                })
                .filter(Optional::isPresent)
                .map(Optional::get);
    }
}
