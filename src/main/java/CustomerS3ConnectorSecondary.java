import com.amazonaws.services.s3.AmazonS3;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutorService;

@Component
@Slf4j
public class CustomerS3ConnectorSecondary extends S3Connector {

    public CustomerS3ConnectorSecondary(
            @Qualifier("s3ClientSecondary") AmazonS3 s3Client,
            @Value("${x.s3.bucket.secondary}") String bucketName,
            ExecutorService s3DownloadThreadPool) {
        super(s3Client, bucketName, s3DownloadThreadPool);
    }
}