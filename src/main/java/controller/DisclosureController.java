package controller;

import io.micrometer.core.annotation.Timed;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.apache.commons.lang3.StringUtils.isBlank;

@RestController
@Slf4j
public class DisclosureController {
    private static final String RESPONSE_BODY_SUCCESS = "0|Success";
    private static final String RESPONSE_BODY_FAIL = "-1|Failure";

    @Autowired
    private DisclosureService disclosureService;

    @Autowired
    private MetricsCollector metricsCollector;

    @Timed
    @Traced
    @PostMapping(path = "/disclosure/metadata", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity saveOrUpdate(@RequestBody DisclosureMetadataRequest request) {
        try {
            log.debug("Disclosure post metadata: {}", request.toString());
            disclosureService.saveOrUpdate(request);
            log.debug("Disclosure metadata saved");
            return ResponseEntity.ok(RESPONSE_BODY_SUCCESS);
        } catch (Exception exception) {
            return errorResponse(exception);
        }
    }

    @Timed
    @Traced
    @GetMapping(path = "/disclosure/metadata")
    public ResponseEntity getMetadata(@RequestParam(required = false) String functionName,
                                      @RequestParam(required = false) String userId) {
        log.debug("Disclosure get metadata: {}, {}", functionName, userId);

        return isBlank(functionName) && isBlank(userId)
                ? ResponseEntity.ok(disclosureService.getFunctionNameList())
                : ResponseEntity.ok(disclosureService.getDisclosureMetaData(functionName, userId));
    }


    @Timed
    @Traced
    @DeleteMapping(path = "/disclosure/metadata")
    public ResponseEntity deleteMetadata(@RequestParam(required = false) String functionName,
                                         @RequestParam(required = false) String userId) {
        log.debug("Disclosure delete metadata: {}, {}", functionName, userId);

        try {
            disclosureService.deleteMetadata(functionName, userId);
            return ResponseEntity.ok(RESPONSE_BODY_SUCCESS);
        } catch (Exception exception) {
            return errorResponse(exception);
        }
    }

    @Timed
    @Traced
    @PostMapping(path = "/disclosure/process", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity processDisclosure(@RequestBody DisclosureProcessRequest request) {
        return doProcess(request.getDsn(), () -> {
            try {
                log.debug("Disclosure post process: {}, {}, {}", request.getFunctionName(), request.getUserId(), request.getDsn());
                metricsCollector.getDisclosureProcessRequestCounter().increment();
                disclosureService.processDisclosure(request);
                log.debug("Disclosure processed successfully");
                metricsCollector.getDisclosureProcessSuccessCounter().increment();
                return ResponseEntity.ok(RESPONSE_BODY_SUCCESS);
            } catch (Exception exception) {
                metricsCollector.getDisclosureProcessFailureCounter().increment();
                return errorResponse(exception);
            }});
    }

    @Timed
    @Traced
    @PostMapping(path = "/disclosure/processRdfs", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity processDisclosureRdfs(@RequestBody DisclosureProcessRdfsRequest request) {
        return doProcess(request.getDsn(), () -> {
            try {
                log.debug("Disclosure post process: {}", request.getDsn());
                metricsCollector.getDisclosureProcessRequestCounter().increment();
                disclosureService.processDisclosure(request);
                log.debug("Disclosure processed successfully");
                metricsCollector.getDisclosureProcessSuccessCounter().increment();
                return ResponseEntity.ok(RESPONSE_BODY_SUCCESS);
            } catch (Exception exception) {
                metricsCollector.getDisclosureProcessFailureCounter().increment();
                return errorResponse(exception);
            }});
    }

    private ResponseEntity errorResponse(Exception exception) {
        String errorMessage = StringUtils.isEmpty(exception.getMessage())
                ? RESPONSE_BODY_FAIL
                : RESPONSE_BODY_FAIL + ":" + exception.getMessage();
        log.error(errorMessage, exception);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(errorMessage);
    }
}
